## Notes
### Add Precommit Hooks:
First add a file in the following repo: touch `.git/hooks/pre-commit` by adding the following line in the file will do the check.
```sh
cargo fmt
exec cargo clippy -- -D warnings
```
Then we must make the file executable with the following command `chmod a+x .git/hooks/pre-commit`

## Creating CMD Args with Clap
Running the following command will disable the cargo's own arguments and will prioratise the package's cmd arguments.
```
cargo run -- somefile --outfile file.out -s
```