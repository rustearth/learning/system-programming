.PHONY: data runapp silent_size show_size broken_pipe cmd_arg

data:
	dd if=/dev/urandom bs=1024 count=128 of=./temp/myfile

runapp:
	cat ./temp/myfile | ./target/debug/pipeviewer > ./temp/myfile2

silent_size:
	echo "a string" | PV_SILENT=something cargo run

show_size:
	echo "a string" | PV_SILENT= cargo run

broken_pipe:
	yes | cargo run | head -n 10000000 > /dev/null


