pub mod args;
pub mod read;
pub mod stats;
pub mod write;

// 16kb with 1024 of total killobites
// This will happen in compile time
const CHUNK_SIZE: usize = 16 * 1024;
