use crate::CHUNK_SIZE;
// Here Read and Write is Trail so that we can use the methods they defined
// Self will also import io itself so that we can use the methods from io
use std::io::{self, BufReader, Read, Result};

// Standard file system file for Read and Write
use std::fs::File;

pub fn read(infile: &str) -> Result<Vec<u8>> {
    let mut reader: Box<dyn Read> = if !infile.is_empty() {
        Box::new(BufReader::new(File::open(infile)?))
    } else {
        Box::new(BufReader::new(io::stdin()))
    };
    let mut buffer = [0; CHUNK_SIZE];

    let num_read = reader.read(&mut buffer)?;

    Ok(Vec::from(&buffer[..num_read]))
}
